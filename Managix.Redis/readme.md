﻿### 开源地址：  
https://github.com/imperugo/StackExchange.Redis.Extensions  
### 文档地址：  
https://imperugo.gitbook.io/stackexchange-redis-extensions/  

### Config:  
```
{
	"Redis": {
		"Password": "my_super_secret_password",
		"AllowAdmin": true,
		"Ssl": false,
		"ConnectTimeout": 6000,
		"ConnectRetry": 2,
		"Database": 0,
		"ServiceName" : "my-sentinel", // In case you are using Sentinel
		"Hosts": [
		{
			"Host": "192.168.0.10",
			"Port": "6379"
		},
		{
			"Host": "192.168.0.11",
			"Port": "6381"
		}]
	},
	"MaxValueLength" = 1024,
	"PoolSize" = 5,
	"KeyPrefix" = "_my_key_prefix_",
}
```