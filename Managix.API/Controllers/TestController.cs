﻿using Managix.Redis.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Managix.API.Controllers
{
    public class ValidatableObjectDto : IValidatableObject
    {
        public string? Name { set; get; }
        public int Age { set; get; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(Name)) yield return new ValidationResult("名字不可为空");
            if (Age <= 18)
            {
                yield return new ValidationResult("未满18周岁不可访问");
            }
        }
    }

    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class TestController : ControllerBase
    {
        // GET: api/<TestController>
        [HttpGet]
        public async Task<string> Get([FromServices] IRedisDatabase _redis)
        {
            try
            {





                string appInfoDtos = await _redis.GetOrCreateAsync<string>($"GetAppList:",
                   async () => await Task.FromResult(Guid.NewGuid().ToString()));


                //var ss = await _redis.AddAsync("test1", Guid.NewGuid().ToString());
                var str = await _redis.GetAsync<string>("test1");
                var str2 = await _redis.SearchKeysAsync("*");
                return DateTime.Now.ToString() + ":" + str;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        /// <summary>
        /// 测试IValidatableObject校验接口
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpGet("TestVali")]
        public ActionResult GetIValidatableObject([FromQuery] ValidatableObjectDto dto)
        {
            HashSet<string> liststr = new HashSet<string>();
            liststr.Add("aaa");
            liststr.Add("bbb");
            liststr.Add("ccc");
            liststr.Add("111");
            liststr.Add("222");
            liststr.Add("333");

            var newList = liststr.OrderBy(_ => Guid.NewGuid());
            string msg = string.Empty;
            foreach (var item in newList)
            {
                msg += item + ";";
            }

            return Content(msg);
        }



        // GET api/<TestController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<TestController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<TestController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<TestController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
