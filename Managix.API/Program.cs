using Managix.API.Common;
using Managix.Redis;
using Managix.Services;
using Microsoft.Extensions.Options;
using NLog.Web;

var builder = WebApplication.CreateBuilder(args);

var hostBuilder = builder.Host;

//配置文件
hostBuilder.ConfigureAppConfiguration((hostBuilderContext, configurationBuilder) =>
{
    //IOptionsSnapshot
    //初始化配置
    //path:文件名
    //optional:是否可选，不存在不报错
    //reloadOnChange:修改之后是否立即从新加载（配置热更新）
    configurationBuilder.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
    configurationBuilder.AddJsonFile("./Configs/jwtconfig.json", optional: true, reloadOnChange: true);
    configurationBuilder.AddJsonFile("./Configs/dbconfig.json", optional: true, reloadOnChange: true);
    configurationBuilder.AddJsonFile("./Configs/cacheconfig.json", optional: true, reloadOnChange: true);
    configurationBuilder.AddJsonFile("./Configs/uploadconfig.json", optional: true, reloadOnChange: true);
    //configurationBuilder.
});
//NLog
hostBuilder.ConfigureLogging(logging =>
{
    logging.ClearProviders();
    logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Information);
    logging.AddConsole();
}).UseNLog();//其中，UseNLog是拓展方法，需要引入NLog.Web.AspNetCore;

NLogBuilder.ConfigureNLog("./Configs/nlog.config");//.GetCurrentClassLogger();


//初始化配置文件
builder.Services.AddConfigs(builder.Configuration);
//jwt
builder.Services.AddJwt();
//容器注入
builder.Services.AddContainer();
//缓存
builder.Services.AddCaChe();
//Swagger
builder.Services.AddSwagger();
//控制器配置
builder.Services.SetController(builder.Configuration);

builder.Services.AddEndpointsApiExplorer();

//builder.Services.AddStackExchangeRedis();

//-------------------------------------------------------
var app = builder.Build();
Service.BaseServiceScope = app.Services.CreateScope();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseRouting();
//认证
app.UseAuthentication();
//授权
app.UseAuthorization();
//静态文件
app.UseStaticFilesMiddle();
//Swagger文档
if (app.Environment.IsDevelopment())
{
    app.UseSwaggerMiddle();
}
app.MapControllers();

app.Run();
